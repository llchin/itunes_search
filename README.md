# itunes_search

an app that let user to search music in iTunes Music.

## development environment

- Flutter 2.10.5 • channel stable • https://github.com/flutter/flutter.git
- Framework • revision 5464c5bac7 (1 year, 1 month ago) • 2022-04-18 09:55:37 -0700
- Engine • revision 57d3bac3dd
- Tools • Dart 2.16.2 • DevTools 2.9.2

## tools and coding practice

- Domain-driven design (DDD): the domains can be seen in the following folder structure:

```
.
└── lib/
    ├── application/        # Application layer saves only event handlers in the form of bloc/cubit
    │   ├── audio_player
    │   ├── localization
    │   └── song
    ├── domain              # Domain layer defines data into entity classes for the app to use
    ├── infrastructure      # Infrastructure layer handles data from outside
    └── presentation/       # Presentation layer is all about Flutter Widgets and its state
        ├── songs/
        │   └── widgets
        └── themes          # Folder for saving commonly used dimensions and styles
```

- Project Structure: Bloc

## running the project

Please run below command in the terminal before you run the project.

```
flutter pub get
flutter pub run build_runner build --delete-conflicting-outputs
```

After that, you can run the project by using the following command:

```
flutter run
```