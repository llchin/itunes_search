/*
SongState holds songs and a sealed class that represent four states our app can be in:
- initial before anything loads
- loading during the API call
- success if the API call is successful
- failure if the API call is failed
*/


part of 'song_cubit.dart';

@freezed
class SongState with _$SongState {
  const SongState._();
  const factory SongState({
    required SongStatus status,
    @Default([]) List<Song> songs,
  }) = _SongState;

  factory SongState.initial() => const SongState(
    status: SongStatus.initial(),
    songs: <Song>[],
  );
}

@freezed
class SongStatus with _$SongStatus {
  const factory SongStatus.initial() = _Initial;
  const factory SongStatus.loading() = _Loading;
  const factory SongStatus.success() = _Success;
  const factory SongStatus.failure() = _Failure;
}