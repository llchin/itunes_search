/*
ChangeLocalState holds the current language option selected by the user.
*/

part of 'localization_cubit.dart';

@immutable
abstract class LocalizationState {}

class LocalizationInitial extends LocalizationState {}

class ChangeLocalState extends LocalizationState {
  final Locale locale;
  ChangeLocalState({required this.locale});
}