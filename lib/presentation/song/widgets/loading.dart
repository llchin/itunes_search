import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      width: 18,
      height: 18,
      child: Center(
        child: CircularProgressIndicator(
          strokeWidth: 3,
          color: Colors.black54,
        ),
      ),
    );
  }
}