import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:itunes_search/application/localization/localization_cubit.dart';

class Language extends StatelessWidget {
  const Language({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LocalizationCubit, LocalizationState>(
        builder: (context, state) {
      if (state is ChangeLocalState) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
                onPressed: () {
                  BlocProvider.of<LocalizationCubit>(context)
                      .changeLanguage('en');
                },
                child: Text(
                  'EN',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                )),
            TextButton(
                onPressed: () {
                  BlocProvider.of<LocalizationCubit>(context)
                      .changeLanguage('zh');
                },
                child: const Text(
                  '中',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                )),
          ],
        );
      }
      return const SizedBox();
    });
  }
}
