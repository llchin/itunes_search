import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:itunes_search/application/localization/app_local.dart';
import 'package:itunes_search/application/song/song_cubit.dart';
import 'package:itunes_search/presentation/song/widgets/audio_player_buttons.dart';
import 'package:itunes_search/presentation/song/widgets/empty_widget.dart';
import 'package:itunes_search/presentation/song/widgets/language.dart';
import 'package:itunes_search/presentation/song/widgets/search_bar.dart';
import 'package:itunes_search/presentation/song/widgets/song_list_view.dart';

class SongPage extends StatelessWidget {
  const SongPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('title'.tr(context)),
        actions: [
          Language(),
        ],
        centerTitle: true,
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: Stack(
        children: [
          Column(
            children: [
              const SearchBar(),
              Expanded(
                child: BlocBuilder<SongCubit, SongState>(
                  builder: (context, state) => state.status.when(
                    initial: () => EmptyWidget(
                      key: Key('__initial__'),
                      text: 'Startasearch'.tr(context),
                    ),
                    loading: () => const Center(
                      child: CircularProgressIndicator(
                        color: Colors.black45,
                      ),
                    ),
                    success: () => state.songs.isNotEmpty
                        ? SongListView(songs: state.songs)
                        : EmptyWidget(
                            key: Key('__noResult__'),
                            text: 'NoResult'.tr(context),
                          ),
                    failure: () => EmptyWidget(
                      key: Key('__searchFailed__'),
                      text: 'SongError'.tr(context),
                    ),
                  ),
                ),
              ),
              const Align(
                alignment: Alignment.bottomCenter,
                child: AudioPlayerButtons(),
              )
            ],
          )
        ],
      ),
    );
  }
}
