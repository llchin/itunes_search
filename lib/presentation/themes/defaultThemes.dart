import 'package:flutter/material.dart';

const defaultPadding = EdgeInsets.all(20.0);
const defaultPaddingValue = 20.0;
const defaultMargin = EdgeInsets.all(8.0);
BorderRadius defaultCircularBorderRadius = BorderRadius.circular(5.0);
const defaultBorderRadiusValue = 5.0;