import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:itunes_search/application/audio_player/audio_player_cubit.dart';
import 'package:itunes_search/application/localization/app_local.dart';
import 'package:itunes_search/application/localization/localization_cubit.dart';
import 'package:itunes_search/application/localization/cache_manager.dart';
import 'package:itunes_search/application/song/song_cubit.dart';

import 'package:itunes_search/infrastructure/song_remote_repository.dart';
import 'package:itunes_search/infrastructure/song_remote_service.dart';
import 'package:itunes_search/presentation/song/song_page.dart';

import 'package:flutter_localizations/flutter_localizations.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await CacheHelper.init();
  runApp(

    MyApp(
      songCubit: SongCubit(
        SongRemoteRepository(
          SongRemoteService(),
        ),
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
    required this.songCubit,
  }) : super(key: key);
  final SongCubit songCubit;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => songCubit,
        ),
        BlocProvider(
          create: (_) => LocalizationCubit()..getSavedLanguage(),
        ),
        BlocProvider(
          create: (_) => AudioPlayerCubit(songCubit: songCubit)..init(),
        )
      ],
      child: BlocBuilder<LocalizationCubit, LocalizationState>(
        builder: (context, state) {
          if (state is ChangeLocalState) {
            return MaterialApp(
              title: 'Itunes Search',
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                primarySwatch: Colors.purple,
              ),
              locale: state.locale,
              localizationsDelegates: const [
                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: const [
                Locale("en", ""),
                Locale("zh", ""),
              ],
              localeResolutionCallback: (currentLocal, supportedLocales) {
                for (var locale in supportedLocales) {
                  if (currentLocal != null &&
                      currentLocal.languageCode == locale.languageCode) {
                    return currentLocal;
                  }
                }
                return supportedLocales.first;
              },
              home: const SongPage(),
            );
          } else {
            return SizedBox();
          }
        }
      )
    );
  }
}